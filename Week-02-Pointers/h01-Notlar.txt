
                                                    1. HAFTA
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

C++ da (b�lgeler)
------------------

1- Statik bellek b�lgesi
2- �al��ma an� y���n�(run time stack)
3- Heap

1-) Statik Bellek B�lgesi
-------------------------

**Global de�i�kenler(bloklardan ba��ms�z her yerde eri�ilebilir)
**Statik lokal de�i�kenler(her yerden eri�ilemez ama 2 side program �al��t��� zaman ya�arlar.)

2-) �al��ma an� y���n�(RTS)
---------------------------

**Lokal de�i�kenler
**Parametreler


                G�STER�C�LER (POINTERS)
                ----------------------

* De�eri getir
& Adresi getir demektir.

int *pi (pointer tan�mlama)

pi = &a; ----> & i�areti a n�n adresini getiriyor ve pi adres tuttu�u i�in a n�n adresini pi ye atar.

cout<<*p --> de�eri yazar y�ld�z olmasa idi adresi yazacakt�.


             G�STER�C�N�N G�STER�C�S� (POINTER TO POINTER)
             --------------------------------------------

int *a;
int **pi;

int b = 10;

pi = &a; ----> do�ru atama.(!!!! direk b atanamaz ��nk� pi 2 ad�ml� pointer oldu�u i�in direk de�ere ba�lanamaz.)

           D�Z�LER
          ---------

int b[3]={5,9,7}; -------> b dizisi bir pointerd�r.
int *p;
p=b; -----> b nin adresi her zaman ilk eleman� g�sterir ve burda p ye b nin ilk eleman�n adresi koyulur.

p++; ------> p nin adresi b nin ilk eleman�n�n adresini tutar ve ++ i�areti int oldu�u i�in sonraki 4 bayta gider yani b dizisinin 2. eleman�na gider.


          HEAP B�LGES�
         -------------

*Anonimdir.
*�smi yoktur sadece adresi vard�r.
*De�er at�lan b�lge isimsiz oldu�u i�in de�eri pointerlarda tutulur(!!!! pointer heap b�lgesinin adresini tutar..)
*new operat�r�yle heap b�lgesinde b�l�m olu�ur.

int *p;
p = new int(100); ---> olu�turma.

delete p; ---> p nin heap b�lgesinde tuttu�u alan yok olur.

       POINTER KARSILASTIRMA
       --------------------

int x=100;
int y=100;

int *p, *r;

p=&x;
r=&y;

if(p == r)cout<<"Esit";
else cout<<"Esit Degil";

**** ESIT DEGIL YAZAR. !!! BURDA ADRES KARSILASTIRMASI YAPMI� OLUYORUZ.

                     SWAP ��LEM�
                     -----------

Sayi *s1 = new Sayi(10);
Sayi *s2 = new Sayi(50);

Sayi *yedek;

yedek = s1;
s1 = s2;             ---------------> SWAP ��LEM�
s2 = yedek;

                        VO�D G�STER�C�S�

!!Void*
!!Herhangi bir �ey g�sterebilir(int, double.....).
!!Kutulama i�lemidir.
