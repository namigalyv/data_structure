#include <iostream>

using namespace std;

double pi=3.14;

void Arttir(){
	static int sayac=0;
	sayac++;
	cout<<sayac;
}

int Topla(int x,int y)
{
	return x+y;
}

class Sayi{
	private:
		int deger;
	public:
		Sayi(int dgr){
			deger=dgr;
		}
		int Deger(){
			return deger;
		}
		friend ostream& operator<<(ostream& ekran,Sayi& sag){
			ekran<<sag.deger;
			return ekran;
		}
};

class Kisi{
	private:
		string isim;
		Sayi *yas;
	public:
		Kisi(string ism){
			isim=ism;
			yas=new Sayi(0);
		}
		~Kisi(){
			delete yas;
		}
		
};
/*
void FF(int &a){
	a=500;
}
*/
/*
void FF(int *a){
	*a==500;
}
*/

int main()
{
	/*Arttir();
	Arttir();
	Arttir();*/
	
	//int a=10;int b=5;
	//cout<<Topla(a,b);
	
	/*int s=10;
	{
		int s=50;
		cout<<s;
	}
	cout<<s;*/
	/*
	int a=10;
	int *p,*r;
	p=&a;
	*/
	
	//cout<<p;
	//cout<<*p;
	
	/*cout<<a<<endl;
	cout<<&a<<endl;
	cout<<p<<endl;
	cout<<*p<<endl;
	cout<<&p<<endl;*/
	
	//a=25;
	//cout<<*p;
	
	//*p=25;
	//cout<<a;
	
	/*
	r=p;
	*r=20;
	cout<<*p;
	*/
	/*
	int *p;
	int a=10;
	p=&a;
	int **pp;
	pp=&p;
	
	cout<<&a<<endl;
	cout<<**pp<<endl;
	cout<<*p<<endl;
	cout<<pp;
	*/
	/*
	int b[3]={5,9,7};
	int *p;
	p=b;
	p++;
	cout<<*p;
	*/
	/*
	int *p;
	p=new int(100);
	delete p;
	*/
	//Kisi *k1=new Kisi("Burak");
	
	//delete k1;
	/*
	for(int i=0;i<5;i++){
		Sayi *s=new Sayi(10);
		cout<<s<<endl;
		delete s;
	}
	*/
	/*
	int x=100;
	int y=100;
	int *p;
	int *r;
	p=&x;
	r=&y;
	
	if(*p==*r){
		cout<<"Esit";
	}
	else{
		cout<<"Esit degil";
	}
	*/
	/*
	Sayi *s1=new Sayi(100);
	Sayi *s2=s1;
	
	delete s1;
	Sayi *s3=new Sayi(500);
	cout<<s2->Deger();
	*/
	/*
	int x=10;
	FF(x);
	cout<<x;
	*/
	/*
	int x=10;
	int *p;
	p=&x;
	FF(p);
	cout<<x;
	*/
	/*
	Sayi *s1=new Sayi(10);
	Sayi *s2=new Sayi(50);
	Sayi *yedek;
	
	cout<<*s1;
	cout<<*s2<<endl;
	
	yedek=s1;
	s1=s2;
	s2=yedek;
	
	cout<<*s1;
	cout<<*s2;
	*/
	
	int a=100;
	void *p;
	p=&a;
	
	p=new Sayi(100);
	//cout<<p->Deger(); hata verir
	cout<<(static_cast<Sayi*>(p))->Deger();
	
	return 0;
}

