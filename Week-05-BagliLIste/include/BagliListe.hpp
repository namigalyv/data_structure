#ifndef BAGLILISTE_HPP
#define BAGLILISTE_HPP

#include "ListeGezici.hpp"

template <typname Nesne>
class BagliListe{
  private:
    Dugum<Nesne> *basDugum;
    int elemanSayisi;

    ListeGezici<Nesne> OncekiniKonumuileBUl(int konum){
      //if(konum<0 || konum>elemanSayisi) throw hata
      int indeks=0;
      Dugum<Nesne> *tmp=basDugum;
      while(tmp->ileri!=NULL && konum !=indeks){
        tmp=tmp->ileri;
        indeks++:
      }
      return ListeGezici<Nesne>(tmp);
    }
  public:
    BagliListe(){
      basDugum=new Dugum<Nesne>();
      elemanSayisi=0;
    }
    int Uzunluk()const{
      return elemanSayisi;
    }
    bool Bosmu(){
      return elemanSayisi==0;
    }
    const Nesne& ilk()const{
      //if(Bosmu()) throw Hata
      return basDugum->ileri->veri;
    }
    const Nesne& son()const{
      //if(Bosmu()) throw Hata
      Nesne sonEleman;
      for(ListeGezici<Nesne> gezici(basDugum->ileri);!gezici.SonaGeldimi();gezici.ilerle()){
        if(gezici.simdiki->ileri==NULL)
          return gezici.simdiki->veri;
      }
      return sonEleman;
    }
    void Ekle(const Nesne& eleman){
      Ekle(elemanSayisi,eleman);
    }
    void Ekle(int konum,const Nesne& eleman){
      ListeGezici<Nesne> onceki=OncekiniKonumuileBUl(konum);
      onceki.simdiki->ileri=new Dugum<Nesne>(eleman,onceki.simdiki->ileri);
      elemanSayisi++;
    }
    void Sil(const Nesne& eleman){
      int konum=Konum(eleman);
      KonumdakiniSil(konum);
    }
    void KonumdakiniSil(int konum){
      ListeGezici<Nesne> onceki=OncekiniKonumuileBUl(konum);
      Dugum<Nesne> *q=onceki.simdiki->ileri;
      onceki.simdiki->ileri=onceki.simdiki->ileri->ileri;
      delete q;
      elemanSayisi--;
    }
    int Konum(const Nesne& eleman){
      //if(Bosmu()) throw Hata
      int indeks=0;
      for(ListeGezici<Nesne> gezici(basDugum->ileri);!gezici.SonaGeldimi();gezici.ilerle()){
        if(gezici.Getir()==eleman){
          return indeks;
        }
        indeks++;
      }
      // throw ElemanYok
    }
    bool Bul(const Nesne& eleman)const{
      for(ListeGezici<Nesne> gezici(basDugum->ileri);!gezici.SonaGeldimi();gezici.ilerle()){
      {
        if(gezici.Getir()==eleman)return true;
      }
      return false;
    }
    friend ostream& operator<<(ostream& ekran,BagliListe& sag){
      if(sag.Bosmu()) ekran<<"Liste Bos"<<endl;
      else{
        for(ListeGezici<Nesne> gezici(sag.basDugum->ileri);!gezici.SonaGeldimi();gezici.ilerle()){
        {
          ekran<<gezici.Getir()<<endl;
        }
      }
      return ekran;
    }
    void Temizle(){
      while(!Bosmu()){
        KonumdakiniSil(0);
      }
    }
    ~BagliListe(){
      Temizle();
      delete basDugum;
    }
};

#endif
