#include <iostream>

using namespace std;


int Faktoriel(int x){
	if(x==0)return 1;
	return x*Faktoriel(x-1);
}

int Fib(int x){
	if(x==0)return 0;
	if(x==1)return 1;
	return Fib(x-1)+Fib(x-2);
}
bool Asal(int sayi,int bolen=2){
	if(sayi==bolen)return true;
	if(sayi==1)return false;
	if(sayi%bolen==0)return false;
	return Asal(sayi,bolen+1);
}
int Us(int x,int y){
	if(y==0)return 1;
	return x*Us(x,y-1);
}
int Uzunluk(string str){
	if(str[0]=='\0')return 0;
	return 1+Uzunluk(str.substr(1));
}
string TersCevir(string str){
	if(str.size()==0)return str;
	return TersCevir(str.substr(1))+str.at(0);
}
int Enbuyuk(int dizi[],int uzunluk,int max=0){
	if(uzunluk==0)return max;
	if(max < dizi[uzunluk-1])max=dizi[uzunluk-1];
	return Enbuyuk(dizi,uzunluk-1,max);
}
bool ikiliArama(int dizi[],int x,int ilk,int son){
	if(ilk>son)return false;
	int orta=(ilk+son)/2;
	if(dizi[orta]==x)return true;
	if(x<dizi[orta])return ikiliArama(dizi,x,ilk,orta-1);
	else return ikiliArama(dizi,x,orta+1,son);
}
void Hanoi(int diskS,char kKule,char aKule,char hKule){
	if(diskS>1){
		Hanoi(diskS-1,kKule,hKule,aKule);
		Hanoi(1,kKule,aKule,hKule);
		Hanoi(diskS-1,aKule,kKule,hKule);
	}
	else{
		cout<<"Bir diski "<<kKule<<" den al "<<hKule<<" ye tasi"<<endl; 
	}
}
void Ali(int);
void Kadir(int devam){
	if(!devam) return;
	cout<<"Kadir"<<endl<<"--------"<<endl;
	Ali(devam-1);
}
void Mehmet(int devam){
	if(!devam) return;
	cout<<"Mehmet"<<endl;
	Kadir(devam);
}
void Ali(int devam){
	if(!devam)return;
	cout<<"Ali"<<endl;
	Mehmet(devam);
}
int main (){
	/* Faktoriel
	int sayi;
	do{
		cout<<"x:";
		cin>>sayi;
		if(sayi>=0) cout<<sayi<<"!:"<<Faktoriel(sayi)<<endl;
	}while(sayi>=0);
	*/
	/* Fibonacci
	int sayi;
	do{
		cout<<"x:";
		cin>>sayi;
		if(sayi>=0) cout<<Fib(sayi)<<endl;
	}while(sayi>=0);
	*/
	/* Asal Sayi
	int sayi;
	do{
		cout<<"x:";
		cin>>sayi;
		if(sayi>0){
			if(Asal(sayi))cout<<"Sayi Asaldir"<<endl;
			else cout<<"Sayi Asal degildir"<<endl;
		}
	}while(sayi>=0);
	*/
	/* Ust alma
	int a,b;
	cout<<"a:";
	cin>>a;
	cout<<"b:";
	cin>>b;
	cout<<a<<"^"<<b<<":"<<Us(a,b);
	*/
	/*
	int a,b;
	cout<<"a:";
	cin>>a;
	cout<<"b:";
	cin>>b;
	cout<<a<<"^"<<b<<":"<<Us(a,b);
	*/
	/*
	int dizi[]={18,5,29,2};
	cout<<dizi;
	*/
	/*
	char *str="Ankara";
	cout<<str;//Ankara
	cout<<*str;//A
	*/
	/* String de karakter sayisi
	string yazi;
	cout<<"Yazi:";
	getline(cin,yazi);
	cout<<"Uzunluk:"<<Uzunluk(yazi);
	*/
	/* String'i ters cevirme
	string yazi;
	cout<<"Yazi:";
	getline(cin,yazi);
	cout<<"Tersi:"<<TersCevir(yazi);
	*/
	/* Dizi'de Enbuyuk Bulma
	int x[]={8400,84,53,5,987,105,1000};
	cout<<Enbuyuk(x,6);
	*/
	/* Ikili Arama
	int x[]={5,89,123,235,568,1024};
	if(ikiliArama(x,500,0,5)) cout<<"Eleman var";
	else cout<<"Eleman yok";
	*/
	/* Hanoi Kuleleri
	int diskS;
	cout<<"Disk Sayisi:";
	cin>>diskS;
	Hanoi(diskS,'A','B','C');
	*/
	int sayi;
	cout<<"Tur:";
	cin>>sayi;
	Ali(sayi);
	
	
	return 0;
}