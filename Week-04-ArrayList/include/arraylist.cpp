#ifndef ARRAYLIST_HPP
#define ARRAYLIST_HPP

#include <iostream>
using namespace std;

template <typename Nesne>
class Arraylist{
	private:
		Nesne *elemanlar;
		int elemanSayisi;
		int kapasite;
		
		void reserve(int yeniKapasite)
		{
			if(kapasite>=yeniKapasite)return;
			
			Nesne *yedek=new Nesne[yeniKapasite];
			for(int i=0;i<elemanSayisi;i++)
			{
				yedek[i]=elemanlar[i];
			}
			if(elemanlar !=NULL) delete [] elemanlar;
			elemanlar=yedek;
			kapasite=yeniKapasite;
		}
	public:
		Arraylist()
		{
			elemanlar=NULL;
			elemanSayisi=0;
			kapasite=0;
		}
		Arraylist(Arraylist<Nesne> &sag)
		{
			elemanSayisi=0;
			elemanlar=new Nesne[sag.length()];
			kapasite=sag.length();
			for(int i=0;i<sag.length();i++)
			{
				add(sag.elementAt(i));
			}
		}
		int length()const
		{
			return elemanSayisi;
		}
		bool isEmpty()const
		{
			return length()==0;
		}
		void add(const Nesne& eleman)
		{
			if(elemanSayisi==kapasite)
			{
				reserve(max(1,2*kapasite));//0*2==0 ise 1 atanır değilse 1 den büyük değer atanır
			}
			elemanlar[elemanSayisi]=eleman;
			elemanSayisi++;
		}
		void insert(int konum,const Nesne& eleman)
		{
			//if(konum <0 || konum>elemanSayisi) throw hata
			if(elemanSayisi==kapasite)
			{
				reserve(max(1,2*kapasite));
			}
			for(int i=elemanSayisi;i>konum;i--)
			{
				elemanlar[i]=elemanlar[i-1];
			}
			elemanlar[konum]=eleman;
			elemanSayisi++;
		}
		void remove(const Nesne& eleman)
		{
			for(int i=0;i<elemanSayisi;i++)
			{
				if(elemanlar[i]==eleman)
				{
					removeAt(i);
					return;
				}
			}
			//throw elemanYokhatasi
		}
		void removeAt(int konum)
		{
			//if(konum<0 || konum>=elemanSayisi) throw hata
			for(int i=konum+1;i<elemanSayisi;i++)
			{
				elemanlar[i-1]=elemanlar[i];
			}
			elemanSayisi--;
		}
		const Nesne& elementAt(int konum)
		{
			//if(konum<0 || konum>=elemanSayisi) throw hata
			return elemanlar[konum];
		}
		int indexOf(const Nesne& eleman)const
		{
			for(int i=0;i<elemanSayisi;i++)
			{
				if(elemanlar[i]==eleman)
				{
					return i;
				}
			}
			//throw elemanYokhatasi
		}
		void clear()
		{
			elemanSayisi=0;
		}
		const Nesne& first()const
		{
			//if(isEmpty()) throw hata
			return elemanlar[0];
		}
		const Nesne& last()const
		{
			//if(isEmpty()) throw hata
			return elemanlar[elemanSayisi-1];
		}
		friend ostream& operator<<(ostream& ekran,Arraylist<Nesne> &sag)//friend olarak eklendigi için private alanlara erişebilir
		{
			for(int i=0;i<sag.elemanSayisi;i++)
			{
				ekran << sag.elemanlar[i]<<endl;
			}
			return ekran;
		}
		~Arraylist()
		{
			if(elemanlar==NULL) return;
			delete [] elemanlar;
		}
	
};

#endif